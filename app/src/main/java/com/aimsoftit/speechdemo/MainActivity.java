package com.aimsoftit.speechdemo;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (PermissionHandler.checkPermission(this, PermissionHandler.RECORD_AUDIO)) {
            startStopService();
        } else {
            PermissionHandler.askForPermission(PermissionHandler.RECORD_AUDIO, this);
        }
    }

    void startStopService() {
        if (isMyServiceRunning(SpeechService.class)) {
            Toast.makeText(this, "Scoped", Toast.LENGTH_SHORT).show();
            stopService(new Intent(MainActivity.this, SpeechService.class));
        } else {
            Toast.makeText(this, "Started", Toast.LENGTH_SHORT).show();
            startService(new Intent(this, SpeechService.class));
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        assert manager != null;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PermissionHandler.RECORD_AUDIO) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startStopService();
                }
            }
        }
    }
//
//    @Override
//    protected void onPause() {
//        if (mSpeechManager != null) {
//            mSpeechManager.destroy();
//            mSpeechManager = null;
//        }
//        super.onPause();
//    }


    @Override
    protected void onStop() {
        super.onStop();
    }
}
