package com.aimsoftit.speechdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(NotificationBroadcastReceiver.class.getSimpleName(), "Service Stops! Oops!!!!");
        Toast.makeText(context, "Receiver", Toast.LENGTH_SHORT).show();

        context.startService(new Intent(context, SpeechService.class));
//        context.startService(new Intent(context, SpeechService.class));

//        String action = intent.getStringExtra("action");
//        if (action.equals("action1")) {
//
//            Toast.makeText(context, "Receiver", Toast.LENGTH_SHORT).show();
//        }
    }
}