package com.aimsoftit.speechdemo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import androidx.core.app.NotificationCompat;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationHandler {
    private static final int NOTIFICATION_CHANNEL_ID = 10001;
    private final static String default_notification_channel_id = "default";
    private final static String NOTIFICATION_CHANNEL_NAME = "AIM_ASSIST_CHANNEL";
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private Context mContext;

    public NotificationHandler(Context context) {
        this.mContext = context;
    }

    public void addNotification(String title, String content) {
        removeNotification();
        Intent notificationIntent = new Intent(mContext, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        //PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, notificationIntent, FLAG_UPDATE_CURRENT);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, notificationIntent, FLAG_UPDATE_CURRENT);

        mNotificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext.getApplicationContext(), default_notification_channel_id);
        mBuilder.setContentTitle(title)
                //.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.exit)
                .setAutoCancel(false)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.home))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                //.addAction(R.drawable.exit, "Singh", pendingIntent)
                .setOngoing(true)
                .build().flags |= Notification.FLAG_NO_CLEAR;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID + "", NOTIFICATION_CHANNEL_NAME, importance);
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID + "");
            assert mNotificationManager != null;
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(NOTIFICATION_CHANNEL_ID, mBuilder.build());
    }

    public void removeNotification() {
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.cancel(NOTIFICATION_CHANNEL_ID);
    }

    public void updateNotificationContent(String content) {
        mBuilder.setContentText(content);
        mNotificationManager.notify(NOTIFICATION_CHANNEL_ID, mBuilder.build());
    }

    public void updateNotificationTitle(String title) {
        mBuilder.setContentTitle(title);
        mNotificationManager.notify(NOTIFICATION_CHANNEL_ID, mBuilder.build());
    }

    public void updateNotificationAction(String actionName) {
        mBuilder.mActions.get(0).title = actionName;
        mNotificationManager.notify(NOTIFICATION_CHANNEL_ID, mBuilder.build());
    }
}