package com.aimsoftit.speechdemo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

public class SpeechService extends RecognitionService {
    public NotificationHandler mNotificationHandler = new NotificationHandler(this);
    // public SpeechRecognizerManager mSpeechManager;

    @Override
    protected void onStartListening(Intent recognizerIntent, Callback listener) {

    }

    @Override
    protected void onCancel(Callback listener) {

    }

    @Override
    protected void onStopListening(Callback listener) {

    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "On Create", Toast.LENGTH_SHORT).show();
        mNotificationHandler.addNotification("AIM Assist", "Online");
        setSpeechListener();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "On onStartCommand", Toast.LENGTH_SHORT).show();
        super.onStartCommand(intent, flags, startId);
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Toast.makeText(this, "On Re-Bind", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        stopSpeech();
        Toast.makeText(this, "onDestroy!", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        sendBroadcast(rootIntent);
        stopSpeech();
        onDestroy();
        super.onTaskRemoved(rootIntent);
        Toast.makeText(this, "onTaskRemoved!", Toast.LENGTH_SHORT).show();
    }

    private void stopSpeech() {
//        if (mSpeechManager != null) {
//            mSpeechManager.destroy();
//            mSpeechManager = null;
//        }
        mNotificationHandler.removeNotification();
    }

    private void setSpeechListener() {
//        mSpeechManager = new SpeechRecognizerManager(this, new SpeechRecognizerManager.onResultsReady() {
//            @Override
//            public void onResults(ArrayList<String> results) {
//                if (results != null && results.size() > 0) {
//                    if (results.size() == 1) {
//                        Log.e("DESTROY", "On Speech Destroy");
//                        Toast.makeText(SpeechService.this, "DESTROY" + results.get(0), Toast.LENGTH_SHORT).show();
//                    } else {
//                        StringBuilder sb = new StringBuilder();
//                        if (results.size() > 5) {
//                            results = (ArrayList<String>) results.subList(0, 5);
//                        }
//                        for (String result : results) {
//                            sb.append(result).append("\n");
//                        }
//                        Toast.makeText(SpeechService.this, sb.toString(), Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(SpeechService.this, getString(R.string.no_results_found), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });

        recognizerManager(this, new onResultsReady() {
            @Override
            public void onResults(ArrayList<String> results) {
                if (results != null && results.size() > 0) {
                    if (results.size() == 1) {
                        Log.e("DESTROY", "On Speech Destroy");
                        Toast.makeText(SpeechService.this, "DESTROY" + results.get(0), Toast.LENGTH_SHORT).show();
                    } else {
                        StringBuilder sb = new StringBuilder();
                        if (results.size() > 5) {
                            results = (ArrayList<String>) results.subList(0, 5);
                        }
                        for (String result : results) {
                            sb.append(result).append("\n");
                        }
                        Toast.makeText(SpeechService.this, sb.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(SpeechService.this, getString(R.string.no_results_found), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    protected AudioManager mAudioManager;
    protected SpeechRecognizer mSpeechRecognizer;
    protected Intent mSpeechRecognizerIntent;
    protected boolean mIsListening;
    private boolean mIsStreamSolo;
    private boolean mMute = true;
    private final static String TAG = "SpeechService--=-=";
    private onResultsReady mListener;

    public void recognizerManager(Context context, onResultsReady listener) {
        try {
            mListener = listener;
        } catch (ClassCastException e) {
            Log.e(TAG, e.toString());
        }
        Toast.makeText(context, "Start!!!!!!!!!!", Toast.LENGTH_SHORT).show();

        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(context);
        mSpeechRecognizer.setRecognitionListener(new SpeechRecognitionListener());
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                context.getPackageName());
        startListening();
    }

    public void listenAgain() {
        if (mIsListening) {
            mIsListening = false;
            mSpeechRecognizer.cancel();
            startListening();
        }
    }

    private void startListening() {
        if (!mIsListening) {
            mIsListening = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                // turn off beep sound
                if (!mIsStreamSolo && mMute) {
                    mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_RING, true);
                    mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
                    mIsStreamSolo = true;
                }
            }
            mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
        }
    }

    public void destroy() {
        mIsListening = false;
        if (!mIsStreamSolo) {
            mAudioManager.setStreamMute(AudioManager.STREAM_NOTIFICATION, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_ALARM, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_MUSIC, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_RING, false);
            mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
            mIsStreamSolo = true;
        }
        Log.d(TAG, "onDestroy");
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.stopListening();
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        }
    }

//    @Override
//    protected void onStartListening(Intent recognizerIntent, Callback listener) {
//
//    }
//
//    @Override
//    protected void onCancel(Callback listener) {
//
//    }
//
//    @Override
//    protected void onStopListening(Callback listener) {
//
//    }

    protected class SpeechRecognitionListener implements RecognitionListener {
        @Override
        public void onBeginningOfSpeech() {
        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {
        }

        @Override
        public synchronized void onError(int error) {
            if (error == SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
                if (mListener != null) {
                    ArrayList<String> errorList = new ArrayList<>(1);
                    errorList.add("ERROR RECOGNIZER BUSY");
                    if (mListener != null)
                        mListener.onResults(errorList);
                }
                return;
            }

            if (error == SpeechRecognizer.ERROR_NO_MATCH) {
                if (mListener != null)
                    mListener.onResults(null);
            }

            if (error == SpeechRecognizer.ERROR_NETWORK) {
                ArrayList<String> errorList = new ArrayList<>(1);
                errorList.add("STOPPED LISTENING");
                if (mListener != null)
                    mListener.onResults(errorList);
            }
            Log.d(TAG, "error = " + error);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listenAgain();
                }
            }, 100);
        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }

        @Override
        public void onPartialResults(Bundle partialResults) {

        }

        @Override
        public void onReadyForSpeech(Bundle params) {
        }

        @Override
        public void onResults(Bundle results) {
            if (results != null && mListener != null)
                mListener.onResults(results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION));
            listenAgain();
        }

        @Override
        public void onRmsChanged(float rmsdB) {
        }
    }

    public boolean isListening() {
        return mIsListening;
    }

    public interface onResultsReady {
        public void onResults(ArrayList<String> results);
    }

    public void mute(boolean mute) {
        mMute = mute;
    }

    public boolean isInMuteMode() {
        return mMute;
    }
}